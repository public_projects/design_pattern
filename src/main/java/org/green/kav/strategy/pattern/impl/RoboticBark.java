package org.green.kav.strategy.pattern.impl;

import org.green.kav.strategy.pattern.IBarkBehaviour;

public class RoboticBark implements IBarkBehaviour {
    @Override
    public void doBehaviour() {
        System.err.println("Sound in speaker");
    }
}
