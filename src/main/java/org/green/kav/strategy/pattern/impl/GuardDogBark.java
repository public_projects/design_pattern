package org.green.kav.strategy.pattern.impl;

import org.green.kav.strategy.pattern.IBarkBehaviour;

public class GuardDogBark implements IBarkBehaviour {
    @Override
    public void doBehaviour() {
        System.err.println("Loud and strong");
    }
}
