package org.green.kav.strategy.pattern;

public interface IRunBehaviour {

    void doBehaviour();
}
