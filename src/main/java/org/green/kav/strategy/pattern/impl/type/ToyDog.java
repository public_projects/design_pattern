package org.green.kav.strategy.pattern.impl.type;

import org.green.kav.strategy.pattern.IBarkBehaviour;
import org.green.kav.strategy.pattern.IRunBehaviour;

public class ToyDog  {
    private IBarkBehaviour barkBehaviour;

    private IRunBehaviour runBehaviour;

    public ToyDog(IBarkBehaviour barkBehaviour, IRunBehaviour runBehaviour) {
        this.barkBehaviour = barkBehaviour;
        this.runBehaviour = runBehaviour;
    }

    public void run(){
        this.runBehaviour.doBehaviour();
    }

    public void bark(){
        this.barkBehaviour.doBehaviour();
    }
}
