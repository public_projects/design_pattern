package org.green.kav.strategy.pattern.main;

import org.green.kav.strategy.pattern.impl.AtleticRun;
import org.green.kav.strategy.pattern.impl.GuardDogBark;
import org.green.kav.strategy.pattern.impl.RoboticBark;
import org.green.kav.strategy.pattern.impl.RoboticRun;
import org.green.kav.strategy.pattern.impl.type.DobbermanDog;
import org.green.kav.strategy.pattern.impl.type.ToyDog;

public class Main {
    public static void main(String[] args) {
        DobbermanDog dobbermanDog = new DobbermanDog(new GuardDogBark(), new AtleticRun());

        dobbermanDog.bark();
        dobbermanDog.run();

        ToyDog toyDog = new ToyDog(new RoboticBark(), new RoboticRun());
        toyDog.bark();
        toyDog.run();

    }
}
