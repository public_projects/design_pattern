package org.green.kav.strategy.pattern.impl;

import org.green.kav.strategy.pattern.IRunBehaviour;

public class AtleticRun implements IRunBehaviour {
    @Override
    public void doBehaviour() {
        System.err.println("It runs fast");
    }
}
