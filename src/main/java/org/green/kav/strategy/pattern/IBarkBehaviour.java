package org.green.kav.strategy.pattern;

public interface IBarkBehaviour {
    void doBehaviour();
}
