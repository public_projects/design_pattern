package org.green.kav.strategy.old;

public class RobotDuck extends Duck {
    @Override
    void quack() {
        System.err.println("Squeak");
    }

    @Override
    void fly() {
        System.err.println("Mechanical flight");
    }
}
