package org.green.kav.strategy.old;

public abstract class Duck {

    abstract void quack();

    abstract void fly();
}
