package org.green.kav.strategy.old;

public class MountainDuck extends Duck {
    @Override
    void quack() {
        System.err.println("Quack");
    }

    @Override
    void fly() {
        System.err.println("Glide");
    }
}
