package org.green.kav.strategy.old;

public class RubberDuck extends Duck{
    @Override
    void quack() {
        System.err.println("Squeak");
    }

    @Override
    void fly() {
        System.err.println("Mechanical flight");
    }
}
